# DNS Balancer

This service automatically perform health checks for a list of remotes and removes/adds dns records once the something is unreachable.
This is the second step to high availability, thus dns failover is still a bad idea (but sometimes you are on low budget, its enough or your load balancer goes down)

But the important question is: Am I using it? Yes!

## Get it

A jar (and a static binary) is build automatically built by my gitlab, check the latest pipeline for artifacts.

### Docker

The recommended way of getting this is docker. Feel free to pull from `gitlab-registry.bixilon.de:443/bixilon/dns-balancer:latest`

## Config

When running dns-balancer, the first parameter is the path to your config file (e.g. `/dns-balancer ./config.yaml`)

### Example configuration

```yaml
services:
  - dns:
      type: A
      domains:
        - one.one.one.one
    remotes:
      - 1.1.1.1
      - 1.0.0.1
    nameserver:
      type: cloudflare
      token: see your cloudflare account
      zone_id: check your cloudflare dashboard for it
    options:
      interval: 5  # checks every 5 seconds
      retries: 3   # tries 2 more times when the first request failed
      retry: 60    # when it failed 3 times, it blocks the host for 60 seconds and then tries again. This should be a multiple of $interval
    check:
      type: http
      timeout: 10            # http timeout
      schema: https          # http or https
      host: one.one.one.one  # http Host header
      path: /                # url
```

This configuration will try to get check https://one.one.one.one:443/. It checks the servers `1.1.1.1` and `1.0.0.1` and update the domain `one.one.one.one` and sets the servers when they are reachable and automatically removes down hosts.

## DNS records

### `A` or `AAAA`

```yaml
type: A   # A (IPv4) or AAAA (IPv6)
domains:
  - one.one.one.one
```

### `MX`

```yaml
type: MX
domains:
  - one.one.one.one
```

To set the priority of MX records, you need to change the remotes:

```yaml
remotes:
  - one.one.one.one           # priority 1 is assumed automatically
  - host: one.zero.zero.one   # This must be a domain name and not an ip address!
    priority: 100
```

### Nameservers

#### `cloudflare`:

```yaml
type: cloudflare
token: see your cloudflare account
zone_id: check your cloudflare dashboard for it
proxied: false
```

### Checks

#### `ping`

```yaml
type: ping
timeout: 10  # timeout in seconds
```

#### `tcp`

```yaml
type: tcp
port: 12345  # tcp port
timeout: 10  # timeout in seconds
```

#### `http`

```yaml
type: http
timeout: 10            # http timeout
schema: https          # http or https
host: one.one.one.one  # http Host header
path: /                # url
expected:
  - "this must be in the response"
  - "and this too"
```

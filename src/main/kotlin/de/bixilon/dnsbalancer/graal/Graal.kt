package de.bixilon.dnsbalancer.graal

import com.oracle.svm.core.annotate.AutomaticFeature
import de.bixilon.dnsbalancer.config.Options
import de.bixilon.dnsbalancer.config.service.Service
import de.bixilon.dnsbalancer.dns.a.ARecord
import de.bixilon.dnsbalancer.dns.aaaa.AAAARecord
import de.bixilon.dnsbalancer.nameserver.cloudflare.CloudflareNameserver
import org.graalvm.nativeimage.hosted.Feature
import org.graalvm.nativeimage.hosted.RuntimeReflection


@AutomaticFeature
internal class RuntimeReflectionRegistrationFeature : Feature {

    override fun beforeAnalysis(access: Feature.BeforeAnalysisAccess) {
        RuntimeReflection.register(Service::class.java.declaredConstructors.first())
        RuntimeReflection.register(Options::class.java.declaredConstructors.first())
        RuntimeReflection.register(ARecord::class.java.declaredConstructors.first())
        RuntimeReflection.register(AAAARecord::class.java.declaredConstructors.first())
        RuntimeReflection.register(CloudflareNameserver::class.java.declaredConstructors.first())
    }
}

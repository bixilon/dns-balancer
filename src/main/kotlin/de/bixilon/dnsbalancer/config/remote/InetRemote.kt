package de.bixilon.dnsbalancer.config.remote

import com.fasterxml.jackson.annotation.JsonCreator
import java.net.InetAddress

data class InetRemote(
    val inet: InetAddress,
) : Remote {

    @JsonCreator
    constructor(hostname: String) : this(InetAddress.getByName(hostname))

    override fun getAddress() = inet

    override fun toString() = inet.toString()
}

package de.bixilon.dnsbalancer.config.remote

import java.net.InetAddress


interface Remote {

    fun getAddress(): InetAddress
}

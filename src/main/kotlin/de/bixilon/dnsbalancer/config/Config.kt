package de.bixilon.dnsbalancer.config

import com.fasterxml.jackson.module.kotlin.readValue
import de.bixilon.dnsbalancer.config.service.Service
import de.bixilon.dnsbalancer.jackson.Jackson
import java.io.File
import java.io.FileInputStream
import java.io.InputStream

data class Config(
    val services: List<Service<*>>,
) {

    companion object {

        fun load(stream: InputStream): Config {
            return Jackson.YAML.readValue<Config>(stream)
        }

        fun load(file: File): Config {
            return load(FileInputStream(file))
        }
    }
}

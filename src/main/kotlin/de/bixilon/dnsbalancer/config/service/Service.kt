package de.bixilon.dnsbalancer.config.service

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import de.bixilon.dnsbalancer.check.Check
import de.bixilon.dnsbalancer.config.Options
import de.bixilon.dnsbalancer.config.remote.Remote
import de.bixilon.dnsbalancer.dns.DnsRecord
import de.bixilon.dnsbalancer.executor.Execution
import de.bixilon.dnsbalancer.jackson.Jackson.MAPPER
import de.bixilon.dnsbalancer.nameserver.Nameserver

class Service<R : Remote>(
    val dns: DnsRecord<R>,
    @JsonProperty("remotes") remotes: List<Any>,
    val nameserver: Nameserver,
    val options: Options = Options(),
    val check: Check,
) {
    @get:JsonIgnore
    val remotes: List<R>

    init {
        val type = MAPPER.typeFactory.constructCollectionType(ArrayList::class.java, dns.getRemoteClass())
        this.remotes = MAPPER.convertValue(remotes, type)
    }

    @JsonIgnore
    val execution = Execution(this)
}

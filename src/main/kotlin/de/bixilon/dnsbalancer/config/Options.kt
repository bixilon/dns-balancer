package de.bixilon.dnsbalancer.config

data class Options(
    val interval: Int = 15,
    val retries: Int = 3,
    val retry: Int = 60,
)

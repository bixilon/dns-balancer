package de.bixilon.dnsbalancer.util

import de.bixilon.dnsbalancer.jackson.Jackson
import de.bixilon.kutil.collections.CollectionUtil.extend
import de.bixilon.kutil.json.JsonObject
import de.bixilon.kutil.primitive.BooleanUtil.decide
import org.apache.hc.core5.net.URIBuilder
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

object HTTP2 {


    fun Map<String, Any>.headers(): Array<String> {
        val headers: MutableList<String> = mutableListOf()

        for ((key, value) in this) {
            headers += key
            headers += value.toString()
        }
        return headers.toTypedArray()
    }

    private fun URIBuilder.addParameters(data: Any?): URIBuilder {
        if (data == null) return this
        val map: JsonObject = Jackson.MAPPER.convertValue(data, Jackson.JSON_MAP_TYPE)
        for ((key, value) in map) {
            if (value == null) continue
            addParameter(key, value.toString())
        }
        return this
    }

    fun <Payload, Response> send(url: String, method: HTTPMethod, data: Payload, bodyPublisher: (Payload) -> String, bodyBuilder: (String) -> Response, headers: Map<String, Any> = emptyMap()): HTTPResponse<Response> {
        val client = HttpClient.newHttpClient()
        val uri = URIBuilder(url)
        var builder = HttpRequest.newBuilder()
            .headers(*headers.headers())

        builder = when (method) {
            HTTPMethod.GET -> {
                uri.addParameters(data)
                builder.GET()
            }

            HTTPMethod.POST -> builder.POST(HttpRequest.BodyPublishers.ofString(bodyPublisher(data)))
            HTTPMethod.PUT -> builder.PUT(HttpRequest.BodyPublishers.ofString(bodyPublisher(data)))
            HTTPMethod.DELETE -> {
                uri.addParameters(data)
                builder.DELETE()
            }
        }

        builder = builder
            .uri(uri.build())


        val response = client.send(builder.build(), HttpResponse.BodyHandlers.ofString())
        return HTTPResponse(response.statusCode(), response.headers(), bodyBuilder(response.body()))
    }


    fun json(url: String, method: HTTPMethod, payload: Any?, headers: Map<String, Any> = emptyMap()): HTTPResponse<Map<String, Any>?> {
        return send(
            url = url,
            method = method,
            data = payload,
            bodyPublisher = { Jackson.MAPPER.writeValueAsString(it) },
            bodyBuilder = { it.isBlank().decide(null) { Jackson.MAPPER.readValue(it, Jackson.JSON_MAP_TYPE) as Map<String, Any> } },
            headers = headers.extend(
                "Content-Type" to "application/json",
                "Accept" to "application/json",
            )
        )
    }

    enum class HTTPMethod {
        GET,
        POST,
        DELETE,
        PUT,
    }
}

package de.bixilon.dnsbalancer.nameserver.cloudflare.records.mx

import de.bixilon.dnsbalancer.nameserver.cloudflare.DnsRecordType
import de.bixilon.dnsbalancer.nameserver.cloudflare.records.base.NextRecord

data class NextMXRecord(
    override val name: String,
    val content: String,
    val priority: Int,
) : NextRecord {
    override val type get() = DnsRecordType.MX
}

package de.bixilon.dnsbalancer.nameserver.cloudflare

import com.fasterxml.jackson.module.kotlin.convertValue
import de.bixilon.dnsbalancer.config.remote.InetRemote
import de.bixilon.dnsbalancer.config.remote.Remote
import de.bixilon.dnsbalancer.dns.DnsRecord
import de.bixilon.dnsbalancer.dns.a.ARecord
import de.bixilon.dnsbalancer.dns.aaaa.AAAARecord
import de.bixilon.dnsbalancer.dns.mx.MXRecord
import de.bixilon.dnsbalancer.dns.mx.MXRemote
import de.bixilon.dnsbalancer.jackson.Jackson
import de.bixilon.dnsbalancer.nameserver.Nameserver
import de.bixilon.dnsbalancer.nameserver.cloudflare.records.a.ExistingARecord
import de.bixilon.dnsbalancer.nameserver.cloudflare.records.a.NextARecord
import de.bixilon.dnsbalancer.nameserver.cloudflare.records.base.ExistingRecord
import de.bixilon.dnsbalancer.nameserver.cloudflare.records.base.NextRecord
import de.bixilon.dnsbalancer.nameserver.cloudflare.records.mx.NextMXRecord
import de.bixilon.dnsbalancer.util.HTTP2
import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.kutil.exception.ExceptionUtil.ignoreAll
import de.bixilon.kutil.json.JsonObject
import de.bixilon.kutil.primitive.BooleanUtil.toBoolean
import de.bixilon.log4fuck.Log
import de.bixilon.log4fuck.LogLevels
import java.io.IOException
import java.net.InetAddress

class CloudflareNameserver(
    val token: String,
    val zoneId: String,
    val proxied: Boolean = false,
) : Nameserver {
    private val dnsUrl = CloudflareAPI.BASE + "/zones/" + zoneId + "/dns_records"


    private fun send(url: String, method: HTTP2.HTTPMethod, payload: Any? = null): JsonObject {
        val response = HTTP2.json(url, method, payload, headers = mapOf("Authorization" to "Bearer ${this.token}"))
        if (response.statusCode == 400) throw IOException("Malformed cloudflare request: $payload: ${response.body}")
        if (response.statusCode != 200) throw IOException("Invalid http response code: ${response.statusCode}")
        if (!response.body!!["success"]!!.toBoolean()) throw IOException("Not a success: ${response.body}")
        return response.body
    }

    private fun listRecords(type: DnsRecordType, domain: String): List<ExistingARecord> {
        val request = DNSQuery(name = domain, type = type)
        val json = send(dnsUrl, HTTP2.HTTPMethod.GET, request)
        return Jackson.MAPPER.convertValue(json["result"]!!)
    }

    private fun removeRecord(record: ExistingRecord) {
        Log.log(LogLevels.DEBUG) { "Cloudflare: Removing dns record: $record" }
        send(dnsUrl + "/${record.id}", HTTP2.HTTPMethod.DELETE)
    }

    private fun addRecord(record: NextRecord) {
        Log.log(LogLevels.DEBUG) { "Cloudflare: Adding dns record: $record" }
        send(dnsUrl, HTTP2.HTTPMethod.POST, record)
    }

    override fun <R : Remote> update(record: DnsRecord<R>, addresses: Set<R>) {
        if (addresses.isEmpty()) {
            throw IllegalStateException("Something does not make sense. All addresses will be removed from $record. Did dns-balancer fall of the internet or are you having a hard time?")
        }

        // https://developers.cloudflare.com/api/operations/dns-records-for-a-zone-create-dns-record
        // https://developers.cloudflare.com/api/operations/dns-records-for-a-zone-delete-dns-record
        // https://developers.cloudflare.com/api/operations/dns-records-for-a-zone-update-dns-record

        when (record) {
            is ARecord -> updateA(record, addresses.unsafeCast())
            is AAAARecord -> updateAAAA(record, addresses.unsafeCast())
            is MXRecord -> updateMX(record, addresses.unsafeCast())
            else -> throw IllegalArgumentException("Can not update record: $record")
        }
    }

    private fun patchRecords(add: Set<NextRecord>, remove: Set<ExistingRecord>) {
        for (record in add) {
            ignoreAll { addRecord(record) }
        }
        if (add.isNotEmpty() && remove.isEmpty()) {
            Log.log(LogLevels.VERBOSE) { "Some records will be removed, waiting 3 seconds because records got added" }
            Thread.sleep(3000)
        }
        for (record in remove) {
            ignoreAll { removeRecord(record) }
        }
    }

    private fun updateAorAAAA(type: DnsRecordType, domain: String, addresses: Set<InetRemote>) {
        val toAdd = addresses.map { it.inet }.toMutableSet()
        val remove: MutableSet<ExistingARecord> = mutableSetOf()
        val add: MutableSet<NextARecord> = mutableSetOf()

        for (existing in listRecords(type, domain)) {
            val inet = InetAddress.getByName(existing.content)
            val removed = toAdd.remove(inet)
            if (removed) continue
            remove += existing
        }
        for (address in toAdd) {
            add += NextARecord(domain, type, address.hostAddress, this.proxied)
        }
        patchRecords(add, remove)
    }

    private fun updateMX(domain: String, addresses: Set<MXRemote>) {
        val toAdd = addresses.toMutableSet()
        val remove: MutableSet<ExistingARecord> = mutableSetOf()
        val add: MutableSet<NextMXRecord> = mutableSetOf()

        for (existing in listRecords(DnsRecordType.MX, domain)) {
            val removed = toAdd.removeIf { it.host == existing.content } // TODO: this does not update the priority
            if (removed) continue
            remove += existing
        }
        for (address in toAdd) {
            add += NextMXRecord(domain, address.host, address.priority)
        }
        patchRecords(add, remove)
    }


    private fun updateA(record: ARecord, addresses: Set<InetRemote>) {
        for (domain in record.domains) {
            updateAorAAAA(DnsRecordType.A, domain, addresses)
        }
    }

    private fun updateAAAA(record: AAAARecord, addresses: Set<InetRemote>) {
        for (domain in record.domains) {
            updateAorAAAA(DnsRecordType.AAAA, domain, addresses)
        }
    }

    private fun updateMX(record: MXRecord, addresses: Set<MXRemote>) {
        for (domain in record.domains) {
            updateMX(domain, addresses)
        }
    }
}

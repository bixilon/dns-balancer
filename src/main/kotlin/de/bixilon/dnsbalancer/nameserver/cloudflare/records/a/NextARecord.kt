package de.bixilon.dnsbalancer.nameserver.cloudflare.records.a

import de.bixilon.dnsbalancer.nameserver.cloudflare.DnsRecordType
import de.bixilon.dnsbalancer.nameserver.cloudflare.records.base.NextRecord

data class NextARecord(
    override val name: String,
    override val type: DnsRecordType,
    val content: String,
    val proxied: Boolean,
) : NextRecord

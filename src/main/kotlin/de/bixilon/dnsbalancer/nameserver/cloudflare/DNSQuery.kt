package de.bixilon.dnsbalancer.nameserver.cloudflare

data class DNSQuery(
    val name: String,
    val type: DnsRecordType,
)

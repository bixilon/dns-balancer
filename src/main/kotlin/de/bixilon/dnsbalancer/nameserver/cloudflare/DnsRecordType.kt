package de.bixilon.dnsbalancer.nameserver.cloudflare

enum class DnsRecordType {
    A,
    AAAA,
    MX,
}

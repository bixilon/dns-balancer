package de.bixilon.dnsbalancer.nameserver.cloudflare.records.base

import de.bixilon.dnsbalancer.nameserver.cloudflare.DnsRecordType

interface NextRecord {
    val name: String
    val type: DnsRecordType

    val comment: String get() = "dns-balancer"
}

package de.bixilon.dnsbalancer.nameserver.cloudflare

object CloudflareAPI {
    const val BASE = "https://api.cloudflare.com/client/v4"
}

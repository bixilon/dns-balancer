package de.bixilon.dnsbalancer.nameserver.cloudflare.records.base

import de.bixilon.dnsbalancer.nameserver.cloudflare.DnsRecordType

interface ExistingRecord {
    val name: String
    val content: String
    val type: DnsRecordType
    val id: String
}

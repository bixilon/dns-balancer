package de.bixilon.dnsbalancer.nameserver.cloudflare.records.a

import de.bixilon.dnsbalancer.nameserver.cloudflare.DnsRecordType
import de.bixilon.dnsbalancer.nameserver.cloudflare.records.base.ExistingRecord

data class ExistingARecord(
    override val name: String,
    override val content: String,
    override val type: DnsRecordType,
    override val id: String,
) : ExistingRecord

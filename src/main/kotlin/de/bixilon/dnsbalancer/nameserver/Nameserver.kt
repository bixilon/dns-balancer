package de.bixilon.dnsbalancer.nameserver

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import de.bixilon.dnsbalancer.config.remote.Remote
import de.bixilon.dnsbalancer.dns.DnsRecord
import de.bixilon.dnsbalancer.nameserver.cloudflare.CloudflareNameserver

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = CloudflareNameserver::class, name = "cloudflare"),
)
interface Nameserver {

    fun <R : Remote> update(record: DnsRecord<R>, addresses: Set<R>)
}

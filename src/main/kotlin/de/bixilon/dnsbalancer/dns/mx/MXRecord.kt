package de.bixilon.dnsbalancer.dns.mx

import de.bixilon.dnsbalancer.dns.DnsRecord

data class MXRecord(
    val domains: List<String>,
) : DnsRecord<MXRemote> {

    override fun getRemoteClass() = MXRemote::class.java
    override fun toString() = "MX $domains"
}

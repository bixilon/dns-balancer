package de.bixilon.dnsbalancer.dns.mx

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import de.bixilon.dnsbalancer.config.remote.Remote
import java.net.InetAddress

data class MXRemote @JsonCreator(mode = JsonCreator.Mode.PROPERTIES) constructor(
    @JsonProperty val host: String,
    @JsonProperty val priority: Int,
) : Remote {

    init {
        if (priority < 0 || priority > Short.MAX_VALUE) throw IllegalArgumentException("MX priority must be a 16 bit value")
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    constructor(host: String) : this(host, 1)

    override fun getAddress(): InetAddress = InetAddress.getByName(host)
}

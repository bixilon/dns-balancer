package de.bixilon.dnsbalancer.dns

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import de.bixilon.dnsbalancer.config.remote.Remote
import de.bixilon.dnsbalancer.dns.a.ARecord
import de.bixilon.dnsbalancer.dns.aaaa.AAAARecord
import de.bixilon.dnsbalancer.dns.mx.MXRecord

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = ARecord::class, name = "A"),
    JsonSubTypes.Type(value = AAAARecord::class, name = "AAAA"),
    JsonSubTypes.Type(value = MXRecord::class, name = "MX"),
)
interface DnsRecord<R : Remote> {

    fun getRemoteClass(): Class<out Remote>
}

package de.bixilon.dnsbalancer.dns.a

import de.bixilon.dnsbalancer.config.remote.InetRemote
import de.bixilon.dnsbalancer.dns.DnsRecord

data class ARecord(
    val domains: List<String>,
) : DnsRecord<InetRemote> {

    override fun getRemoteClass() = InetRemote::class.java
    override fun toString() = "A $domains"
}

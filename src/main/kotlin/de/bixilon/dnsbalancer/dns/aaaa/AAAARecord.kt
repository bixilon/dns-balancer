package de.bixilon.dnsbalancer.dns.aaaa

import de.bixilon.dnsbalancer.config.remote.InetRemote
import de.bixilon.dnsbalancer.dns.DnsRecord

data class AAAARecord(
    val domains:List<String>,
) : DnsRecord<InetRemote> {

    override fun getRemoteClass() = InetRemote::class.java
    override fun toString() = "AAAA $domains"
}

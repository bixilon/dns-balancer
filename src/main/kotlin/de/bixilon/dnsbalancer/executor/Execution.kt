package de.bixilon.dnsbalancer.executor

import de.bixilon.dnsbalancer.config.remote.Remote
import de.bixilon.dnsbalancer.config.service.Service
import de.bixilon.kutil.collections.map.LockMap
import de.bixilon.kutil.concurrent.pool.ThreadPool
import de.bixilon.kutil.concurrent.worker.unconditional.UnconditionalWorker
import de.bixilon.kutil.exception.ExceptionUtil.retry
import de.bixilon.kutil.time.TimeUtil.nanos
import de.bixilon.kutil.unit.UnitFormatter.formatNanos
import de.bixilon.log4fuck.Log
import de.bixilon.log4fuck.LogLevels
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

class Execution<R : Remote>(
    val service: Service<R>,
) {
    private val pool = ThreadPool(service.remotes.size, name = "${service.dns}#%d")
    private var previous: MutableSet<R> = mutableSetOf()
    private val blocked: LockMap<R, AtomicInteger> = LockMap()
    private val blockInterval = service.options.let { it.retry / it.interval + (if (it.retry % it.interval == 0) 0 else 1) }

    private fun check(remote: R): Boolean {
        val inet = remote.getAddress()
        try {
            retry(service.options.retries) { service.check.isValid(inet) }
            return true
        } catch (error: Throwable) {
            Log.log(LogLevels.DEBUG, error)
            Log.log(LogLevels.WARN) { "Blocked $remote for ${service.options.retry} seconds: ${error.message}" }
            blocked[remote] = AtomicInteger(blockInterval)
            return false
        }
    }

    private fun decrementBlocking(address: R): Boolean {
        val blocked = blocked[address] ?: return false
        if (blocked.decrementAndGet() > 0) {
            return true
        }
        this.blocked -= address
        return false
    }

    private fun check(remote: R, addresses: MutableSet<R>) {
        if (decrementBlocking(remote)) return
        val passed = check(remote)
        if (!passed) return
        addresses += remote
    }

    fun check() {
        Log.log(LogLevels.VERBOSE) { "Checking ${service.dns}" }
        val output: MutableSet<R> = Collections.synchronizedSet(HashSet(service.remotes.size))

        val start = nanos()
        val worker = UnconditionalWorker(pool = this.pool, threadSafe = false, forcePool = true)
        for (remote in service.remotes) {
            worker += { check(remote, output) }
        }
        worker.work()
        val delta = nanos() - start

        if (output == previous) {
            if (output.isEmpty()) {
                Log.log(LogLevels.WARN) { "Remotes for service ${service.dns} are empty!" }
            } else {
                Log.log(LogLevels.VERBOSE) { "Remotes for service ${service.dns} did not change (check took ${delta.formatNanos()}): $output" }
            }
            return
        }
        val level = if (output.size < previous.size) LogLevels.WARN else LogLevels.INFO
        Log.log(level) { "Remotes for service ${service.dns} changed (check took ${delta.formatNanos()}): (previous=$previous, now=$output)" }
        try {
            retry(3) { service.nameserver.update(service.dns, output) }
        } catch (error: Throwable) {
            Log.log(level) { "Could not update remotes for service ${service.dns}. Changes not applied and reverted: $error" }
            return
        }
        this.previous = output
    }
}

package de.bixilon.dnsbalancer.check.http

import de.bixilon.dnsbalancer.check.Check
import de.bixilon.kutil.exception.FastException
import org.apache.hc.client5.http.classic.methods.HttpGet
import org.apache.hc.client5.http.config.ConnectionConfig
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder
import org.apache.hc.client5.http.impl.io.BasicHttpClientConnectionManager
import org.apache.hc.core5.http.HttpHost
import org.apache.hc.core5.http.io.entity.EntityUtils
import org.apache.hc.core5.http.message.BasicHeader
import org.apache.hc.core5.util.Timeout
import java.net.InetAddress


class HttpCheck(
    val schema: HttpSchema,
    val port: Int = if (schema == HttpSchema.HTTP) 80 else 443,
    val host: String?,
    val path: String = "",
    val timeout: Int = 10,
    val expected: List<String> = emptyList(),
) : Check {
    private val config = ConnectionConfig.custom()
        .setConnectTimeout(Timeout.ofMilliseconds(timeout * 333L)) // TODO: bug: it always takes 3x the time!
        .setSocketTimeout(Timeout.ofMilliseconds(timeout * 333L))
        .build()


    override fun isValid(address: InetAddress) {
        val connectionManager = BasicHttpClientConnectionManager().apply {
            connectionConfig = config
        }

        val client = HttpClientBuilder.create()
            .setUserAgent("dns-balancer")
            .setConnectionManager(connectionManager)
            .build()

        val request = HttpGet("${schema.name.lowercase()}://${host ?: address.hostAddress}$path").apply {
            addHeader(BasicHeader("Host", host ?: address.hostAddress))
            scheme = schema.name.lowercase()
            path = this@HttpCheck.path
        }
        val response = client.execute(HttpHost(schema.name.lowercase(), address, host ?: address.hostAddress, port), request)
        if (response.code != 200) throw FastException("Invalid http response code: ${response.code}")

        if (expected.isEmpty()) return
        val content = EntityUtils.toString(response.entity)
        for (string in expected) {
            if (string !in content) {
                throw FastException("Expected string \"$string\" in response!")
            }
        }
    }
}

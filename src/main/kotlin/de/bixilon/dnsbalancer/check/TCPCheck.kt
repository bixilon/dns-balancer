package de.bixilon.dnsbalancer.check

import de.bixilon.kutil.exception.FastException
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket

class TCPCheck(
    val port: Int,
    val timeout: Int = 10,
) : Check {

    init {
        if (port <= 0 || port > 65535) throw IllegalArgumentException("Invalid port: $port")
    }

    override fun isValid(address: InetAddress) {
        val socket = Socket()
        socket.tcpNoDelay = false
        socket.connect(InetSocketAddress(address, port), timeout + 1000)
        if (!socket.isConnected) throw FastException("Could not connect to socket!")
        socket.close()
    }
}

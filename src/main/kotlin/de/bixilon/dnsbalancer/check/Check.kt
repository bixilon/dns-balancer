package de.bixilon.dnsbalancer.check

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import de.bixilon.dnsbalancer.check.http.HttpCheck
import java.net.InetAddress

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = PingCheck::class, name = "ping"),
    JsonSubTypes.Type(value = TCPCheck::class, name = "tcp"),
    JsonSubTypes.Type(value = HttpCheck::class, name = "http"),
)
interface Check {

    fun isValid(address: InetAddress)
}

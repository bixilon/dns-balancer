package de.bixilon.dnsbalancer.check

import de.bixilon.kutil.exception.FastException
import java.net.InetAddress

class PingCheck(
    val timeout: Int = 10,
) : Check {

    override fun isValid(address: InetAddress) {
        if (address.isReachable(timeout * 1000)) {
            return
        }
        throw FastException("Host unreachable!")
    }
}

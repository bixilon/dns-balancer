package de.bixilon.dnsbalancer

import com.fasterxml.jackson.module.kotlin.readValue
import de.bixilon.dnsbalancer.config.Config
import de.bixilon.dnsbalancer.jackson.Jackson
import de.bixilon.kutil.concurrent.pool.DefaultThreadPoolConfig
import de.bixilon.kutil.concurrent.schedule.RepeatedTask
import de.bixilon.kutil.concurrent.schedule.TaskScheduler
import de.bixilon.log4fuck.Log
import de.bixilon.log4fuck.LogLevels
import java.io.FileInputStream
import kotlin.system.exitProcess

object DNSBalancer {

    fun main(configPath: String) {
        Log.log(LogLevels.INFO) { "Starting DNS Balancer with config: $configPath" }

        val config = Jackson.YAML.readValue<Config>(FileInputStream(configPath))
        Log.log(LogLevels.INFO) { "Loaded config file with ${config.services.size} services!" }

        val threads = minOf(config.services.size, DefaultThreadPoolConfig.threads) + 1
        Log.log(LogLevels.VERBOSE) { "Setting kutil thread pool size to $threads threads!" }
        DefaultThreadPoolConfig.threads = threads

        Log.log(LogLevels.INFO) { "Starting service checks..." }
        for (service in config.services) {
            val task = RepeatedTask(service.options.interval * 1000) { service.execution.check() }
            task.lastExecution = -1 // TODO: kutil 1.27: task.enqueue()
            TaskScheduler += task
        }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        if (args.size != 1) throw IllegalArgumentException("Usage: ./application <config>")
        val configPath = args[0]
        try {
            main(configPath)
        } catch (error: Throwable) {
            error.printStackTrace()
            exitProcess(1)
        }
    }
}

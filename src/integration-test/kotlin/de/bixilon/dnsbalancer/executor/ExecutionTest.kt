package de.bixilon.dnsbalancer.executor

import de.bixilon.dnsbalancer.Inet
import de.bixilon.dnsbalancer.check.DummyCheck
import de.bixilon.dnsbalancer.config.remote.Remote
import de.bixilon.dnsbalancer.config.service.Service
import de.bixilon.dnsbalancer.dns.a.ARecord
import de.bixilon.dnsbalancer.nameserver.DummyNameserver
import de.bixilon.kutil.cast.CastUtil.cast
import de.bixilon.kutil.cast.CastUtil.unsafeCast
import de.bixilon.kutil.reflection.ReflectionUtil.getFieldOrNull
import org.testng.annotations.Test
import java.util.concurrent.atomic.AtomicInteger
import kotlin.test.assertEquals

@Test
class ExecutionTest {
    private val PREVIOUS = Execution::class.java.getFieldOrNull("previous")!!
    private val BLOCKED = Execution::class.java.getFieldOrNull("blocked")!!

    private val Execution<*>.previous: Set<Remote> get() = PREVIOUS.get(this).cast()
    private val Execution<*>.blocked: Map<Remote, AtomicInteger> get() = BLOCKED.get(this).cast()

    fun `always failing`() {
        val check = DummyCheck(true)
        val service = Service(ARecord(listOf("example.com")), listOf(Inet.LOCALHOST, Inet.BIXILON_DE, Inet.INVALID_LOCAL), DummyNameserver(), check = check)
        service.execution.check()
        service.execution.assertBlocked(setOf(Inet.LOCALHOST, Inet.BIXILON_DE, Inet.INVALID_LOCAL), 4)
        assertEquals(service.execution.previous, emptySet())
    }

    fun `always succeeding`() {
        val check = DummyCheck(false)
        val service = Service(ARecord(listOf("example.com")), listOf(Inet.LOCALHOST, Inet.BIXILON_DE, Inet.INVALID_LOCAL), DummyNameserver(), check = check)
        service.execution.check()
        assertEquals(service.execution.blocked, emptyMap())
        assertEquals(service.execution.previous, setOf(Inet.LOCALHOST, Inet.BIXILON_DE, Inet.INVALID_LOCAL))
    }

    fun `fail one host succeeding`() {
        val check = DummyCheck(false, mutableSetOf(Inet.LOCALHOST.inet))
        val service = Service(ARecord(listOf("example.com")), listOf(Inet.LOCALHOST, Inet.BIXILON_DE, Inet.INVALID_LOCAL), DummyNameserver(), check = check)
        service.execution.check()
        service.execution.assertBlocked(setOf(Inet.LOCALHOST), 4)
        assertEquals(check.failed[Inet.LOCALHOST.inet]!!.get(), 3) // failed 3 times
        assertEquals(service.execution.previous, setOf(Inet.BIXILON_DE, Inet.INVALID_LOCAL))
    }

    fun `host unblocking after x intervals`() {
        val check = DummyCheck(false, mutableSetOf(Inet.LOCALHOST.inet))
        val service = Service(ARecord(listOf("example.com")), listOf(Inet.LOCALHOST, Inet.BIXILON_DE, Inet.INVALID_LOCAL), DummyNameserver(), check = check)
        service.execution.check()
        check.failOn -= Inet.LOCALHOST.inet
        service.execution.check();service.execution.check();service.execution.check()
        service.execution.assertBlocked(setOf(Inet.LOCALHOST), 1)
        service.execution.check()
        assertEquals(service.execution.previous, setOf(Inet.LOCALHOST, Inet.BIXILON_DE, Inet.INVALID_LOCAL))
    }

    @Test(enabled = false)
    fun `half succeeding`() {
        val check = DummyCheck(false, mutableSetOf(), 2)
        val service = Service(ARecord(listOf("example.com")), listOf(Inet.LOCALHOST, Inet.BIXILON_DE, Inet.INVALID_LOCAL), DummyNameserver(), check = check)
        service.execution.check()
        assertEquals(service.execution.blocked.unsafeCast(), mapOf(Inet.INVALID_LOCAL to AtomicInteger(4)))
        assertEquals(service.execution.previous, setOf(Inet.LOCALHOST, Inet.BIXILON_DE))
    }

    private fun Execution<*>.assertBlocked(hosts: Set<Remote>, intervals: Int) {
        assertEquals(blocked.size, hosts.size, "Differs: $hosts vs $blocked")

        for (host in hosts) {
            val blocked = blocked[host] ?: throw AssertionError("Host $host is not blocked!")
            assertEquals(blocked.get(), intervals, "Block time of $host differs: ${blocked.get()} vs $intervals")
        }
    }
}

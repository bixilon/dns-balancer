package de.bixilon.dnsbalancer.check.http

import de.bixilon.dnsbalancer.Inet
import org.testng.Assert.assertThrows
import org.testng.Assert.assertTrue
import org.testng.annotations.Test
import kotlin.system.measureTimeMillis

@Test(groups = ["check"])
class HttpCheckTest {

    fun `query bixilon_de`() {
        val check = HttpCheck(HttpSchema.HTTPS, 443, "bixilon.de", "/en", timeout = 10, expected = listOf("Hi there!"))
        check.isValid(Inet.BIXILON_DE.inet)
    }

    fun `query bixilon_de in local network`() {
        val check = HttpCheck(HttpSchema.HTTPS, 443, "bixilon.de", "/en", timeout = 3, expected = listOf("Hi there!"))
        val time = measureTimeMillis { assertThrows { check.isValid(Inet.INVALID_LOCAL.inet) } }
        assertTrue(time < 11000)
    }
}

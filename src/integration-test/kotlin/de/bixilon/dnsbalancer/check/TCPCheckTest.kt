package de.bixilon.dnsbalancer.check

import de.bixilon.dnsbalancer.Inet
import org.testng.Assert.assertThrows
import org.testng.Assert.assertTrue
import org.testng.annotations.Test
import kotlin.system.measureTimeMillis

@Test(groups = ["check"])
class TCPCheckTest {

    fun `ping 1111 port 53`() {
        TCPCheck(53).isValid(Inet.ONE_ONE_ONE_ONE.inet)
    }

    fun `ping 1111 at port 12345`() {
        assertThrows { TCPCheck(12345, 3).isValid(Inet.INVALID_LOCAL.inet) }
    }

    fun `verify timeout`() {
        val time = measureTimeMillis { assertThrows { TCPCheck(12345, 3).isValid(Inet.INVALID_LOCAL.inet) } }
        assertTrue(time < 3500)
    }
}

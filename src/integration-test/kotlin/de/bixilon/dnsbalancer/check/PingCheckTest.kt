package de.bixilon.dnsbalancer.check

import de.bixilon.dnsbalancer.Inet
import org.testng.Assert
import org.testng.Assert.assertThrows
import org.testng.annotations.Test
import kotlin.system.measureTimeMillis

@Test(groups = ["check"])
class PingCheckTest {

    fun `ping localhost`() {
        PingCheck(1).isValid(Inet.LOCALHOST.inet)
    }

    fun `ping invalid local ip`() {
        assertThrows { PingCheck(1).isValid(Inet.INVALID_LOCAL.inet) }
    }


    fun `verify timeout`() {
        val time = measureTimeMillis { assertThrows { PingCheck(3).isValid(Inet.INVALID_LOCAL.inet) } }
        Assert.assertTrue(time < 3500)
    }
}

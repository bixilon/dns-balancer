package de.bixilon.dnsbalancer.check

import de.bixilon.kutil.exception.FastException
import java.net.InetAddress
import java.util.concurrent.atomic.AtomicInteger

class DummyCheck(
    var fail: Boolean = true,
    val failOn: MutableSet<InetAddress> = mutableSetOf(),
    var count: Int = Int.MAX_VALUE,
) : Check {
    val failed: MutableMap<InetAddress, AtomicInteger> = HashMap()

    override fun isValid(address: InetAddress) {
        if (fail) throw FastException("Intended failure!")
        if (address !in failOn) return
        this.failed.getOrPut(address) { AtomicInteger(0) }.incrementAndGet()
        throw FastException("Intended!")
    }
}

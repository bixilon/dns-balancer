package de.bixilon.dnsbalancer

import de.bixilon.dnsbalancer.config.remote.InetRemote
import java.net.InetAddress

object Inet {
    val LOCALHOST = InetAddress.getByName("127.0.0.1").remote()
    val INVALID_LOCAL = InetAddress.getByName("192.168.99.99").remote()
    val BIXILON_DE = InetAddress.getByName("79.143.188.245").remote()
    val ONE_ONE_ONE_ONE = InetAddress.getByName("1.1.1.1").remote()


    private fun InetAddress.remote(): InetRemote = InetRemote(this)
}

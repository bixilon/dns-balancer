package de.bixilon.dnsbalancer.nameserver

import de.bixilon.dnsbalancer.config.remote.Remote
import de.bixilon.dnsbalancer.dns.DnsRecord

class DummyNameserver : Nameserver {
    var addresses: Set<Remote> = emptySet()

    override fun <R : Remote> update(record: DnsRecord<R>, addresses: Set<R>) {
        this.addresses = addresses
    }
}

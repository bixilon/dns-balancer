package de.bixilon.dnsbalancer.config

import de.bixilon.dnsbalancer.config.remote.InetRemote
import de.bixilon.dnsbalancer.dns.mx.MXRemote
import org.testng.Assert.assertEquals
import org.testng.annotations.Test
import java.net.InetAddress

@Test(groups = ["config"])
class ConfigTest {

    fun example1() {
        val config = Config.load(ConfigTest::class.java.getResourceAsStream("/example1.yaml")!!)
        assertEquals(config.services.size, 2)
        // TODO: test more
        assertEquals(config.services.first().remotes, listOf(InetRemote(InetAddress.getByName("207.180.229.178")), InetRemote(InetAddress.getByName("176.126.87.173"))))
    }

    fun example2() {
        val config = Config.load(ConfigTest::class.java.getResourceAsStream("/example2.yaml")!!)
        assertEquals(config.services.first().remotes, listOf(MXRemote("1.1.1.1", 1), MXRemote("1.0.0.1", 100)))
    }
}

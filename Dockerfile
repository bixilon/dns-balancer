FROM alpine:latest

COPY dns-balancer /usr/bin/dns-balancer
RUN chmod 555 /usr/bin/dns-balancer

CMD ["/usr/bin/dns-balancer"]
